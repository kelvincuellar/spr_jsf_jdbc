package com.kz.facturacion.utils;

import com.kz.usespring.models.Cliente;
import java.util.List;

public interface Dao<T> {
    public void create(T t);
    public List<T> read();
    public T readBy(long id);
    public void update(T t);
    public void delete(T t);
}
