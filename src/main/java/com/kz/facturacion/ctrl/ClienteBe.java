package com.kz.facturacion.ctrl;

import com.kz.usespring.models.Cliente;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.springframework.stereotype.Component;
import com.kz.facturacion.utils.Dao;
import com.kz.facturacion.utils.Enlace;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;

@Component
@RequestScoped
@ManagedBean(name = "kzBean")
public class ClienteBe implements Serializable {

    @ManagedProperty(value = "#{daoCliente}")
    //@Autowired
    private Dao daoCliente;

    public void setDaoCliente(Dao daoCliente) {
        this.daoCliente = daoCliente;
    }

    public Dao getDaoCliente() {
        return daoCliente;
    }

    public ClienteBe() {
    }

    public List<Cliente> listar() {
        try {
            List<Cliente> ls = daoCliente.read();
            return ls;
        } catch (Exception x) {
            return null;
        }
    }

    public void mostrarClientes() throws JRException, IOException {

        File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("exports/otrocliente.jasper"));
        Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("param", "KZ");
        JasperPrint jasperprint = JasperFillManager.fillReport(jasper.getPath(), parametros, Enlace.conecta());

        /*Visualizar en el navegador*/
        byte[] bytes = JasperRunManager.runReportToPdf(jasper.getPath(), parametros, Enlace.conecta());
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.setContentType("application/pdf");
        response.setContentLength(bytes.length);
        ServletOutputStream os = response.getOutputStream();
        os.write(bytes, 0, bytes.length);
        os.flush();
        os.close();
        FacesContext.getCurrentInstance().responseComplete();

        /*Descargar pdf con JSF*/
//        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//        response.addHeader("Content-disposition", "attachment; filename=reporte.pdf");
//        ServletOutputStream printer = response.getOutputStream();
//        JasperExportManager.exportReportToPdfStream(jasperprint, printer);
//        FacesContext.getCurrentInstance().responseComplete();
    }
}
