package com.kz.usespring.models;

import java.util.List;

/**
 * @author Elena
 */
public class ModoPago {
    private long numPago;
    private String nombre;
    private String otrosDetalles;
    private List<Factura> lsFacts;

    public long getNumPago() {
        return numPago;
    }

    public void setNumPago(long numPago) {
        this.numPago = numPago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOtrosDetalles() {
        return otrosDetalles;
    }

    public void setOtrosDetalles(String otrosDetalles) {
        this.otrosDetalles = otrosDetalles;
    }

    public List<Factura> getLsFacts() {
        return lsFacts;
    }

    public void setLsFacts(List<Factura> lsFacts) {
        this.lsFacts = lsFacts;
    }
        
}
