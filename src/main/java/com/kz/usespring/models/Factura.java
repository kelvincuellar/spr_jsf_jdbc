package com.kz.usespring.models;

import java.util.Date;
import java.util.List;

/**
 * @author Elena
 */
public class Factura {

    private long numFactura;
    private String codigo;
    private Date fecha;
    private Cliente client;
    private ModoPago modPago;
    private List<Detalle> lsDet;

    public long getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(long numFactura) {
        this.numFactura = numFactura;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Cliente getClient() {
        return client;
    }

    public void setClient(Cliente client) {
        this.client = client;
    }

    public ModoPago getModPago() {
        return modPago;
    }

    public void setModPago(ModoPago modPago) {
        this.modPago = modPago;
    }

    public List<Detalle> getLsDet() {
        return lsDet;
    }

    public void setLsDet(List<Detalle> lsDet) {
        this.lsDet = lsDet;
    }
}
